import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;


class YellingTest {
	@Test	
	public void testOnePersonIsYelling() {
		String[] OnePersonIsYelling = { "Peter" };
		System.out.println(Yelling.scream(OnePersonIsYelling));
		assertEquals("Peter is yelling", Yelling.scream(OnePersonIsYelling ));
		
	}	
	
	@Test	
	public void testNoBodyIsYelling() {
		String[] noPeopleYelling = { "null" };
		String name = Yelling.scream(noPeopleYelling);
		String result = name.replaceFirst("null", "Nobody");
		System.out.println(result);
		assertEquals("Nobody is yelling", result);

		
	}
	@Test	
	public void testUpperYelling() {
		String[] upperYelling = { "PETER" };
		System.out.println(Yelling.scream(upperYelling).toUpperCase());
		assertEquals("PETER IS YELLING", Yelling.scream(upperYelling).toUpperCase());


	}
	@Test
	public void testTwoPeopleYelling() {
		String[] twoPeopleYelling1 = { "Albert", "Pritesh" };
		System.out.println(Yelling.scream(twoPeopleYelling1));
		assertEquals("Albert and Pritesh  are yelling", Yelling.scream(twoPeopleYelling1 ));

			}
	@Test
	public void testMoreThanTwoYelling() {
		String[] moreThanTwoPeopleYelling = { "Petera", "Kadeem", "Albert" };
		System.out.println(Yelling.scream(moreThanTwoPeopleYelling));
		assertEquals("Petera,Kadeem,and Albert are yelling", Yelling.scream(moreThanTwoPeopleYelling ));

			}
	@Test	
	public void testLotofPeople() {

		String[] lotOfPeople1 = { "Peter", "EMAD" };
		System.out.println(Yelling.scream(lotOfPeople1));
		assertEquals("Peter and EMAD  are yelling", Yelling.scream(lotOfPeople1 ));


		String[] lotOfPeople2 = { "Petera", "EMAD", "Albert" };
		System.out.println(Yelling.scream(lotOfPeople2));
		assertEquals("Petera and Albert are yelling. SO IS EMAD!", Yelling.scream(lotOfPeople2 ));


	    String[] lotOfPeople3 = { "EMAD", "Peter", "Albert", "Kadeem" };
		System.out.println(Yelling.scream(lotOfPeople3));
		assertEquals("Peter ,Albert ,and Kadeemare yelling. SO IS EMAD!", Yelling.scream(lotOfPeople3 ));

			}




}